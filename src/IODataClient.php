<?php

namespace SaintSystems\OData;

use Closure;
use SaintSystems\OData\Query\Builder;
use SaintSystems\OData\Query\IGrammar;
use SaintSystems\OData\Query\IProcessor;

interface IODataClient
{
    /**
     * Gets the IAuthenticationProvider for authenticating HTTP requests.
     *
     * @return Callable|null
     */
    public function getAuthenticationProvider():? Callable;

    /**
     * Gets the base URL for requests of the client.
     *
     * @return string
     */
    public function getBaseUrl(): string;

    /**
     * Gets the IHttpProvider for sending HTTP requests.
     *
     * @return IHttpProvider
     */
    public function getHttpProvider(): IHttpProvider;

    /**
     * Begin a fluent query against an OData service
     *
     * @param string $entitySet
     *
     * @return Builder
     */
    public function from(string $entitySet): Builder;

    /**
     * Begin a fluent query against an odata service
     *
     * @param array $properties
     *
     * @return Builder
     */
    public function select(array $properties = []): Builder;

    /**
     * Get a new query builder instance.
     *
     * @return Builder
     */
    public function query(): Builder;

    /**
     * @param $requestUri
     * @param array $bindings
     *
     * @return IODataRequest|array
     */
    public function get($requestUri, array $bindings = []): IODataRequest|array;

    /**
     * Get the query grammar used by the connection.
     *
     * @return IGrammar
     */
    public function getQueryGrammar(): IGrammar;

    /**
     * Set the query grammar used by the connection.
     *
     * @param IGrammar $grammar
     *
     * @return void
     */
    public function setQueryGrammar(IGrammar $grammar): void;

    /**
     * Get the query post processor used by the connection.
     *
     * @return IProcessor
     */
    public function getPostProcessor(): IProcessor;

    /**
     * Set the query post processor used by the connection.
     *
     * @param IProcessor $processor
     *
     * @return void
     */
    public function setPostProcessor(IProcessor $processor): void;
}
