<?php

namespace SaintSystems\OData;

use SaintSystems\OData\Exception\ODataException;
use SaintSystems\OData\Query\Builder;
use SaintSystems\OData\Query\Grammar;
use SaintSystems\OData\Query\IGrammar;
use SaintSystems\OData\Query\IProcessor;
use SaintSystems\OData\Query\Processor;

class ODataClient implements IODataClient
{
    /**
     * The base service URL. For example, "https://services.odata.org/V4/TripPinService/"
     *
     * @var string
     */
    private string $baseUrl;

    /**
     * The IAuthenticationProvider for authenticating request messages.
     *
     * @var Callable|null
     */
    private $authenticationProvider;

    /**
     * The IHttpProvider for sending HTTP requests.
     *
     * @var IHttpProvider
     */
    private IHttpProvider $httpProvider;

    /**
     * The query grammar implementation.
     *
     * @var IGrammar
     */
    protected IGrammar $queryGrammar;

    /**
     * The query post processor implementation.
     *
     * @var IProcessor
     */
    protected IProcessor $postProcessor;

    /**
     * The return type for the entities
     *
     * @var string|null
     */
    private ?string $entityReturnType = null;

    /**
     * Constructs a new ODataClient.
     *
     * @param string $baseUrl
     *   The base service URL.
     * @param callable|null $authenticationProvider
     *   The IAuthenticationProvider for authenticating request messages.
     * @param IHttpProvider|null $httpProvider
     *   The IHttpProvider for sending requests.
     *
     * @throws ODataException
     */
    public function __construct(
        string        $baseUrl,
        Callable      $authenticationProvider = null,
        IHttpProvider $httpProvider = null
    ) {
        $this->setBaseUrl($baseUrl);
        $this->authenticationProvider = $authenticationProvider;
        $this->httpProvider = $httpProvider ?: new GuzzleHttpProvider();

        // We need to initialize a query grammar and the query post processors
        // which are both very important parts of the OData abstractions,
        // so we initialize these to their default values while starting.
        $this->useDefaultQueryGrammar();

        $this->useDefaultPostProcessor();
    }

    /**
     * Set the query grammar to the default implementation.
     *
     * @return void
     */
    public function useDefaultQueryGrammar(): void
    {
        $this->queryGrammar = $this->getDefaultQueryGrammar();
    }

    /**
     * Get the default query grammar instance.
     *
     * @return IGrammar
     */
    protected function getDefaultQueryGrammar(): IGrammar
    {
        return new Grammar;
    }

    /**
     * Set the query post processor to the default implementation.
     *
     * @return void
     */
    public function useDefaultPostProcessor(): void
    {
        $this->postProcessor = $this->getDefaultPostProcessor();
    }

    /**
     * Get the default post processor instance.
     *
     * @return IProcessor
     */
    protected function getDefaultPostProcessor(): IProcessor
    {
        return new Processor();
    }

    /**
     * Gets the IAuthenticationProvider for authenticating requests.
     *
     * @return Callable|null
     */
    public function getAuthenticationProvider():? Callable
    {
        return $this->authenticationProvider;
    }

    /**
     * Gets the base URL for requests of the client.
     *
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * Sets the base URL for requests of the client.
     *
     * @param mixed $value
     *
     * @throws ODataException
     */
    public function setBaseUrl(mixed $value)
    {
        if (empty($value)) {
            throw new ODataException(Constants::BASE_URL_MISSING);
        }

        $this->baseUrl = rtrim($value, '/') . '/';
    }

    /**
     * Gets the IHttpProvider for sending HTTP requests.
     *
     * @return IHttpProvider
     */
    public function getHttpProvider(): IHttpProvider
    {
        return $this->httpProvider;
    }

    /**
     * Begin a fluent query against an odata service
     *
     * @param string $entitySet
     *
     * @return Builder
     */
    public function from(string $entitySet): Builder
    {
        return $this->query()->from($entitySet);
    }

    /**
     * Begin a fluent query against an odata service
     *
     * @param array $properties
     *
     * @return Builder
     */
    public function select(array $properties = []): Builder
    {
        $properties = is_array($properties) ? $properties : func_get_args();

        return $this->query()->select($properties);
    }

    /**
     * Get a new query builder instance.
     *
     * @return Builder
     */
    public function query(): Builder
    {
        return new Builder(
            $this, $this->getQueryGrammar(), $this->getPostProcessor()
        );
    }

    /**
     * Run a GET HTTP request against the service.
     *
     * @param string $requestUri
     * @param array $bindings
     *
     * @return IODataRequest|array
     */
    public function get($requestUri, array $bindings = []): IODataRequest|array
    {
        return $this->request(HttpMethod::GET, $requestUri);
    }

    /**
     * Run a POST request against the service.
     *
     * @param string $requestUri
     * @param mixed  $postData
     *
     * @return IODataRequest|array
     */
    public function post(string $requestUri, mixed $postData): IODataRequest|array
    {
        return $this->request(HttpMethod::POST, $requestUri, $postData);
    }

    /**
     * Run a PATCH request against the service.
     *
     * @param string $requestUri
     * @param mixed  $body
     *
     * @return IODataRequest|array
     */
    public function patch(string $requestUri, mixed $body): IODataRequest|array
    {
        return $this->request(HttpMethod::PATCH, $requestUri, $body);
    }

    /**
     * Run a DELETE request against the service.
     *
     * @param string $requestUri
     *
     * @return IODataRequest|array
     */
    public function delete(string $requestUri): IODataRequest|array
    {
        return $this->request(HttpMethod::DELETE, $requestUri);
    }

    /**
     * Return an ODataRequest
     *
     * @param string $method
     * @param string $requestUri
     * @param mixed|null $body
     *
     * @return IODataRequest|array
     *
     * @throws ODataException
     */
    public function request(string $method, string $requestUri, mixed $body = null): IODataRequest|array
    {
        $request = new ODataRequest($method, $this->baseUrl.$requestUri, $this, $this->entityReturnType);

        if ($body) {
            $request->attachBody($body);
        }

        // TODO: find a better solution for this
        /*
        if ($method === 'PATCH' || $method === 'DELETE') {
            $request->addHeaders(array('If-Match' => '*'));
        }
         */

        return $request->execute();
    }

    /**
     * Get the query grammar used by the connection.
     *
     * @return IGrammar
     */
    public function getQueryGrammar(): IGrammar
    {
        return $this->queryGrammar;
    }

    /**
     * Set the query grammar used by the connection.
     *
     * @param  IGrammar  $grammar
     *
     * @return void
     */
    public function setQueryGrammar(IGrammar $grammar): void
    {
        $this->queryGrammar = $grammar;
    }

    /**
     * Get the query post processor used by the connection.
     *
     * @return IProcessor
     */
    public function getPostProcessor(): IProcessor
    {
        return $this->postProcessor;
    }

    /**
     * Set the query post processor used by the connection.
     *
     * @param IProcessor $processor
     *
     * @return void
     */
    public function setPostProcessor(IProcessor $processor): void
    {
        $this->postProcessor = $processor;
    }

    /**
     * Set the entity return type
     *
     * @param string $entityReturnType
     */
    public function setEntityReturnType(string $entityReturnType)
    {
        $this->entityReturnType = $entityReturnType;
    }
}
