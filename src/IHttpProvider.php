<?php

namespace SaintSystems\OData;

use Psr\Http\Message\ResponseInterface;

interface IHttpProvider
{

    /**
     * Sends the request.
     *
     * @param HttpRequestMessage $request The HttpRequestMessage to send.
     *
     * @return ResponseInterface
     *   An object or array of objects
     */
    public function send(HttpRequestMessage $request): ResponseInterface;

}
