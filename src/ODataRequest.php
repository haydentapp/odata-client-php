<?php

namespace SaintSystems\OData;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use SaintSystems\OData\Exception\ODataException;

/**
 * The base request class.
 */
class ODataRequest implements IODataRequest
{
    /**
     * The URL for the request
     *
     * @var string
     */
    protected string $requestUrl;

    /**
     * The Guzzle client used to make the HTTP request
     *
     * @var Client
     */
    protected Client $http;

    /**
     * An array of headers to send with the request
     *
     * @var array(string => string)
     */
    protected array $headers;

    /**
     * The body of the request (optional)
     *
     * @var string|Stream|null
     */
    protected string|Stream|null $requestBody;

    /**
     * The type of request to make ("GET", "POST", etc.)
     *
     * @var string
     */
    protected string $method;

    /**
     * True if the response should be returned as
     * a stream
     *
     * @var bool
     */
    protected bool $returnsStream;

    /**
     * The return type to cast the response as
     *
     * @var mixed
     */
    protected mixed $returnType;

    /**
     * The timeout, in seconds
     *
     * @var integer
     */
    protected int $timeout;

    /**
     * The ODataClient.
     *
     * @var IODataClient
     */
    private IODataClient $client;

    /**
     * Constructs a new ODataRequest object
     *
     * @param string $method
     *   The HTTP method to use, e.g. "GET" or "POST"
     * @param string $requestUrl
     *   The URL for the OData request
     * @param IODataClient $client
     *   The ODataClient used to make the request
     * @param mixed|null $returnType
     *   Optional return type for the OData request (defaults to Entity)
     *
     * @throws ODataException
     */
    public function __construct(
        string       $method,
        string       $requestUrl,
        IODataClient $client,
        mixed $returnType = null
    ) {
        $this->method = $method;
        $this->requestUrl = $requestUrl;
        $this->client = $client;
        $this->setReturnType($returnType);

        if (empty($this->requestUrl)) {
            throw new ODataException(Constants::REQUEST_URL_MISSING);
        }
        $this->timeout = 0;
        $this->returnsStream = false;
        $this->requestBody = false;
        $this->headers = $this->getDefaultHeaders();
    }

    /**
     * Sets the return type of the response object
     *
     * @param mixed $returnClass The object class to use
     *
     * @return ODataRequest
     */
    public function setReturnType(mixed $returnClass): static
    {
        if (is_null($returnClass)) return $this;
        $this->returnType = $returnClass;
        if (strcasecmp($this->returnType, 'stream') == 0) {
            $this->returnsStream  = true;
        } else {
            $this->returnsStream = false;
        }
        return $this;
    }

    /**
     * Adds custom headers to the request
     *
     * @param array $headers
     *   An array of custom headers
     *
     * @return ODataRequest
     */
    public function addHeaders(array $headers): static
    {
        $this->headers = array_merge($this->headers, $headers);
        return $this;
    }

    /**
     * Get the request headers
     *
     * @return array
     *   An array of headers
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Attach a body to the request. Will JSON encode
     * any SaintSystems\OData\Entity objects as well as arrays
     *
     * @param mixed $obj
     *   The object to include in the request
     *
     * @return ODataRequest
     */
    public function attachBody(mixed $obj): static
    {
        // Attach streams & JSON automatically
        if (is_string($obj) || is_a($obj, 'GuzzleHttp\\Psr7\\Stream')) {
            $this->requestBody = $obj;
        }
        // JSON-encode the model object's property dictionary
        else if (is_object($obj) && method_exists($obj, 'getProperties')) {
            $class = get_class($obj);
            $class = explode("\\", $class);
            $model = strtolower(end($class));

            $body = $this->flattenDictionary($obj->getProperties());
            $this->requestBody = "{" . $model . ":" . json_encode($body) . "}";
        }
        // By default, JSON-encode (i.e. arrays)
        else {
            $this->requestBody = json_encode($obj);
        }
        return $this;
    }

    /**
     * Get the body of the request
     *
     * @return string
     *   The request body of any type
     */
    public function getBody(): string
    {
        return $this->requestBody;
    }

    /**
     * Sets the timeout limit of the HTTP request
     *
     * @param integer $timeout
     *   The timeout in ms
     *
     * @return ODataRequest
     */
    public function setTimeout(int $timeout): static
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * Executes the HTTP request using Guzzle
     *
     * @return ResponseInterface|string|array|ODataResponse
     *   An object or array of objects of class $returnType
     *
     * @throws ODataException
     *   If response is invalid
     */
    public function execute(): ResponseInterface|string|array|ODataResponse
    {
        if (empty($this->requestUrl))
        {
            throw new ODataException(Constants::REQUEST_URL_MISSING);
        }

        $request = $this->getHttpRequestMessage();
        $request->body = $this->requestBody;

        $this->authenticateRequest($request);

        $result = $this->client->getHttpProvider()->send($request);

        //Send back the bare response
        if ($this->returnsStream) {
            return $result;
        }

        if ($this->isAggregate()) {
            return (string) $result->getBody();
        }

        // Wrap response in ODataResponse layer
        try {
            $response = new ODataResponse(
                $this,
                (string) $result->getBody(),
                $result->getStatusCode(),
                $result->getHeaders()
            );
        } catch (\Exception $e) {
            throw new ODataException(Constants::UNABLE_TO_PARSE_RESPONSE);
        }

        // If no return type is specified, return DynamicsResponse
        $returnObj = $response;

        $returnType = $this->returnType ?? Entity::class;

        if ($returnType) {
            $returnObj = $response->getResponseAsObject($returnType);
        }
        return $returnObj;
    }

    /**
     * Get a list of headers for the request
     *
     * @return array
     *   The headers for the request
     */
    private function getDefaultHeaders(): array
    {
        $headers = [
            //RequestHeader::HOST => $this->client->getBaseUrl(),
            RequestHeader::CONTENT_TYPE => ContentType::APPLICATION_JSON,
            RequestHeader::ODATA_MAX_VERSION => Constants::MAX_ODATA_VERSION,
            RequestHeader::ODATA_VERSION => Constants::ODATA_VERSION,
            RequestHeader::PREFER => Constants::ODATA_MAX_PAGE_SIZE_DEFAULT,
            RequestHeader::USER_AGENT => 'odata-sdk-php-' . Constants::SDK_VERSION,
            //RequestHeader::AUTHORIZATION => 'Bearer ' . $this->accessToken
        ];

        if (!$this->isAggregate()) {
            $headers[RequestHeader::ACCEPT] = ContentType::APPLICATION_JSON ;
        }
        return $headers;
    }

    /**
     * Gets the HttpRequestMessage representation of the request.
     *
     * @retyrns HttpRequestMessage
     *  The HttpRequestMessage representation of the request.
     */
    public function getHttpRequestMessage(): HttpRequestMessage
    {
        $request = new HttpRequestMessage(new HttpMethod($this->method), $this->requestUrl);

        $this->addHeadersToRequest($request);

        return $request;
    }

    /**
     * Returns whether or not the request is an OData aggregate request ($count, etc.)
     */
    private function isAggregate(): bool
    {
        return str_contains($this->requestUrl, '/$count');
    }

    /**
     * Adds all of the headers from the header collection to the request.
     *
     * @param HttpRequestMessage $request
     *   The HttpRequestMessage representation of the request.
     */
    private function addHeadersToRequest(HttpRequestMessage $request)
    {
        $request->headers = array_merge($this->headers, $request->headers);
    }

    /**
     * Adds the authentication header to the request.
     *
     * @param HttpRequestMessage $request
     *   The representation of the request.
     *
     * @return void
     */
    private function authenticateRequest(HttpRequestMessage $request): void
    {
        $authenticationProvider = $this->client->getAuthenticationProvider();
        if ( ! is_null($authenticationProvider) && is_callable($authenticationProvider)) {
            $authenticationProvider($request);
        }
    }

    /**
     * Flattens the property dictionaries into JSON-friendly arrays
     *
     * @param mixed $obj
     *   The object to flatten
     *
     * @return array flattened object
     */
    protected function flattenDictionary(mixed $obj): array
    {
        foreach ($obj as $arrayKey => $arrayValue) {
            if (method_exists($arrayValue, 'getProperties')) {
                $data = $arrayValue->getProperties();
                $obj[$arrayKey] = $data;
            } else {
                $data = $arrayValue;
            }
            if (is_array($data)) {
                $newItem = $this->flattenDictionary($data);
                $obj[$arrayKey] = $newItem;
            }
        }
        return $obj;
    }
}
