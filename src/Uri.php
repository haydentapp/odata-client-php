<?php

namespace SaintSystems\OData;

class Uri
{
    const URI_PARTS = [
        'scheme',
        'host',
        'port',
        'user',
        'pass',
        'path',
        'query',
        'fragment'
    ];

    /**
     * @var string
     */
    public string $scheme;

    /**
     * @var string
     */
    public string $host;

    /**
     * @var integer
     */
    public int $port;

    /**
     * @var string
     */
    public string $user;

    /**
     * @var string
     */
    public string $pass;

    /**
     * @var string
     */
    public string $path;

    /**
     * @var string
     */
    public string $query;

    /**
     * @var string
     */
    public string $fragment;

    /**
     * @var string|array|int|null|false
     */
    private string|array|int|null|false $parsed;

    /**
     * @param string $uri
     */
    public function __construct($uri = null)
    {
        if ($uri == null) return;
        $uriParsed = parse_url($uri);
        $this->parsed = $uriParsed;
        foreach(self::URI_PARTS as $uriPart) {
            if (isset($uriParsed[$uriPart])) {
                $this->$uriPart = $uriParsed[$uriPart];
            }
        }
    }

    public function __toString()
    {
        return http_build_url($this->parsed);
    }
}
